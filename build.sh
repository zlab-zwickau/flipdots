#!/bin/bash

# A simple, automatic caching build system for Atmel AVR systems
# 2020 Florian Daßler (florian.dassler@s2020.tu-chemnitz.de)
# Rev 4


# Configuration
mcu="atmega168"
fcpu="16000000L"

port="/dev/ttyUSB0"
baud="19200"
programmer="arduino"
baud_term="9600"

cc="/usr/bin/avr-gcc"
cppc="/usr/bin/avr-g++"
objcopy="/usr/bin/avr-objcopy"
size="/usr/bin/avr-size"
avrdude="/usr/bin/avrdude"

cflags="-c -Wall -std=c11 -mmcu=$mcu -Os -DF_CPU=$fcpu -DON_AVR -flto"
cppflags="-c -Wall -std=c++14 -mmcu=$mcu -Os -DF_CPU=$fcpu -DON_AVR -flto"

ld="/usr/bin/avr-g++"
ldflags="-mmcu=$mcu -flto"

bin="bin/firmware"

#gendir="generated"

srcdir="src"
objdir="bin"

# zusätzliche Object-Files
objfiles=""

mkdir "$objdir" >/dev/null 2>&1

execute_echo() {
    echo $ $@
    $@
    exitcode=$?

    if [ "$exitcode" -ne 0 ]; then
        echo " --- ERROR DURING BUILD ---"
        echo "  The previous command exited with Code $exitcode"
        exit $exitcode
    fi
}

if [ "$1" == "clean" ]; then
    echo "Cleaning up..."
    execute_echo rm -r "$objdir"
    execute_echo rm -f "$bin.*"
    exit 0
fi

# C-Dateien
sourcefiles=$(find "$srcdir" -name "*.c" -printf "%p ")
# sourcefiles="$sourcefiles $(find "$gendir" -name "*.c" -printf "%p ")"
# C++-Dateien
sourcefiles="$sourcefiles $(find "$srcdir" -name "*.cpp" -printf "%p ")"

files_changed="false"

echo "Compiling..."

read -ra arr <<< "$sourcefiles"
for srcfile in "${arr[@]}"; do

    srcfile_path=$(realpath "$srcfile") # voller Pfad der C-Datei

    # wenn Dateiname mit . beginnt ignorieren
    if echo "$srcfile_path" | grep -q "\/\."; then
        continue
    fi

    hash=$(md5sum "$srcfile" | head -c 8) # kurzer Hash des Quellcodes

    objfile="${objdir}/${hash}.o"

    objfiles="$objfiles $objfile"

    # Objekt-Datei nicht existent --> Build dieser notwendig
    if [ ! -f "$objfile" ]; then

        # C-Datei?
        if [ "$(echo -n $srcfile | tail -c 2)" = ".c" ]; then
            execute_echo "$cc" "$cflags" -o "$objfile" "$srcfile"
            files_changed="true"
        fi

        # CPP-Datei?
        if [ "$(echo -n $srcfile | tail -c 4)" = ".cpp" ]; then
            execute_echo "$cppc" "$cppflags" -o "$objfile" "$srcfile"
            files_changed="true"
        fi
    fi
done

if [ "$files_changed" == "true" ]; then
    echo "Linking everything together..."

    execute_echo "$ld" "$objfiles" "$add_objfiles" "$ldflags" -o "$bin.elf"  
    execute_echo "$objcopy" -O ihex -j .data -j .text "$bin.elf" "$bin.hex"
    execute_echo "$size" --mcu="$mcu" -C "$bin.elf"

    echo "Cleaning up..."

    objfiles_all=$(find "$objdir" -name "*.o" -printf "%p ")

    read -ra arr <<< "$objfiles_all"
    for objfile_all in "${arr[@]}"; do
        # Datei wird nicht mehr benötigt
        if echo "$objfiles" |  grep -vFq "$objfile_all"; then
            execute_echo rm "$objfile_all"
        fi
    done

    echo "Sucessfully built $bin!"
else
    echo "No rebuild necessary."
    echo "Run $0 clean if you still want to rebuild the application."
fi

# run if run option is set
if [ "$1" == "flash" ]; then
    echo " --- Uploading ./${bin}... --- "

    execute_echo "$avrdude" -v "-p$mcu" "-c$programmer" "-P$port" "-b$baud" -D "-Uflash:w:$bin.hex:i"

    echo "--- Upload exited with error code ${?} ---"

    if [ "${?}" == "0" ]; then
        echo "--- Opening Terminal with $baud_term Baud ---"

        execute_echo stty -F "$port" "$baud_term"
        cat "$port"
    fi
fi
