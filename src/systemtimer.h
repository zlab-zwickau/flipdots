
#ifndef __SYSTEMTIMER_H__
#define __SYSTEMTIMER_H__

#include <stdint.h>

#define CLOCK_CYCLES_TO_MICROSECONDS(a) ( (a) / CLOCK_CYCLES_PER_MICROSECOND() )
#define CLOCK_CYCLES_PER_MICROSECOND() ( F_CPU / 1000000L )

uint32_t systemtimer_millis();
void systemtimer_init();

#endif