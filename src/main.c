#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "usart.h"
#include "io.h"
#include "systemtimer.h"
#include "font5x7.h"

#define SPI_CLK B,5
#define SPI_DO  B,3
#define SPI_CS B,2

#define WRITE   B,1
#define POWER   B,0

#define NUM_MODULES 2
#define DISPLAY_WIDTH (28 * NUM_MODULES)
#define DISPLAY_HEIGHT 16
#define DISPLAY_HEIGHT_BYTES (DISPLAY_HEIGHT / 8)

uint8_t flipdot_framebuffer[DISPLAY_WIDTH * DISPLAY_HEIGHT_BYTES];
static uint8_t flipdot_framebuffer_current[DISPLAY_WIDTH * DISPLAY_HEIGHT_BYTES]; // zum diffen

static int16_t scroll_offset;

static struct {
    uint8_t row: 4;
    uint8_t color: 1;
    uint8_t col: 5;
    uint8_t mid: 6;
} display_control;

static const uint8_t col_lut[] PROGMEM =  { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                                            0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
                                            0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
                                            0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F };

static void display_control_transfer() {
    uint8_t * bytes = (uint8_t *) &display_control;
    CLEAR(SPI_CS);

    SPDR = bytes[1];
    while(!(SPSR & (1 << SPIF)));

    SPDR = bytes[0];
    while(!(SPSR & (1 << SPIF)));

    SET(SPI_CS);
}

static inline void set_pixel(uint16_t x, uint16_t y, uint8_t color) {
    display_control.mid = (x / 28) + 1;
    display_control.col = pgm_read_byte(&col_lut[x % 28]);
    display_control.row = y;
    display_control.color = !color; // low zum setzen
    display_control_transfer();
    _delay_us(50);

    SET(WRITE);
    _delay_us(600);
    CLEAR(WRITE);
}

// aktulisiert 1 Byte aus Framebuffer
static inline void update_byte(uint16_t byte) {
    // Zuerst gesamtes Byte überprüfen, wenn gleich muss ja nichts gemacht werden
    if(flipdot_framebuffer != flipdot_framebuffer_current) {
        // 2 Bytes pro Spalte
        uint8_t col = byte / DISPLAY_HEIGHT_BYTES;

        for(uint8_t i = 0; i < 8; i++) {
            uint8_t mask = (1 << i);
            if((flipdot_framebuffer[byte] ^ flipdot_framebuffer_current[byte]) & mask) {
                set_pixel(col, ((byte % DISPLAY_HEIGHT_BYTES) * 8) + i, !!(flipdot_framebuffer[byte] & mask));
            }
        }

        flipdot_framebuffer_current[byte] = flipdot_framebuffer[byte];      
    }
}

void flipdot_update_display() {
    for(uint16_t i = 0; i < sizeof(flipdot_framebuffer); i++) {
        update_byte(i);
    }
}

void flipdot_set_char(uint16_t x, uint16_t y, uint8_t chr) {
    // Offset in Framebuffer, 1 zwischendrin frei lassen zwischen Chars
    int16_t byte_offset = ((x * (FONT_CHAR_WIDTH + 1) + scroll_offset) * DISPLAY_HEIGHT_BYTES) + y;
    // Offset in Font-Array
    uint16_t font_offset = ((uint16_t) chr) * FONT_CHAR_WIDTH;
    for(uint8_t i = 0; i < FONT_CHAR_WIDTH; i++) {
        int16_t index = byte_offset + (i * DISPLAY_HEIGHT_BYTES);
        if(index < 0 || index > (sizeof(flipdot_framebuffer) - 1)) continue;
        flipdot_framebuffer[index] = pgm_read_byte(&font_data[font_offset + i]);
    }
}

void flipdot_print(uint8_t x, uint8_t y, uint8_t * text) {
    while (*text != '\0') {
        flipdot_set_char(x++, y, *text++);
    }
}

void flipdot_clear() {
    memset(flipdot_framebuffer, 0, sizeof(flipdot_framebuffer));
}

int main() {
    
    usart_init();

    // SPI
    SET_OUTPUT(SPI_CLK);
    SET_OUTPUT(SPI_DO);
    SET_OUTPUT(SPI_CS);
    SET(SPI_CS);
    SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR0); // Master, 1 MHz

    display_control_transfer();

    SET_OUTPUT(WRITE);
    SET_OUTPUT(POWER);
    _delay_ms(100);
    SET(POWER);
    _delay_ms(500); // Damit Kondi laden kann

    memset(flipdot_framebuffer, 0, sizeof(flipdot_framebuffer));
    memset(flipdot_framebuffer_current, 255, sizeof(flipdot_framebuffer)); // um Update zu erzwingen
    flipdot_update_display();


    char itoa_buf[32];
    uint8_t c = 0;
    while(true) {
        _delay_ms(50);

        scroll_offset = -(c % 64) + 28;
        flipdot_clear();
        flipdot_print(0, (c / 64) & 1, "z-Labor");
        flipdot_update_display(); 

        /*_delay_ms(100);
        scroll_offset = 10;
        itoa(c, itoa_buf, 10);
        flipdot_clear();
        flipdot_print(0, 0, itoa_buf);
        flipdot_update_display(); */
        
        c++;
    }

    return 0;
}