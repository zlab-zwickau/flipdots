
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

static char itoa_buf[sizeof(int) * 8 + 1];

void usart_init() {
    cli();

    // Set baud rate to 9600 (with 16MHz clock)
	// (See "Examples of Baud Rate Setting" in the ATmega328P datasheet)
    UBRR0H = (uint8_t)(103 >> 8);
    UBRR0L = (uint8_t)(103);

    // Enable receiver and transmitter
    UCSR0B = _BV(RXEN0) | _BV(TXEN0);

    // Set frame format: 8 data, 1 stop bit
    UCSR0C = (3 << UCSZ00);
    UCSR0C &= ~(1 << USBS0);

    sei();
}

void usart_transmit(uint8_t data) {
    // Wait for empty transmit buffer
    while ( !( UCSR0A & (1 << UDRE0)) );

    // Put data into buffer, sends the data
    UDR0 = data;
}

void print(const char* data) {
    while (*data != '\0')
        usart_transmit(*data++); 
}

// Progmem
void print_p(const char* data) {
    while (pgm_read_byte(data) != 0x00)
        usart_transmit(pgm_read_byte(data++));
}

void print_int(int number, int base) {
    itoa(number, itoa_buf, base);
    print(itoa_buf);
}

void print_bin(uint8_t number, uint8_t bits) {
    number = number << (8 - bits);

    for(uint8_t i = 0; i < bits; i++) {
        usart_transmit(number & 0x80 ? '1' : '0');
        number = number << 1;
    }
}

void println(char* string) {
    print(string);
    usart_transmit('\n');
}

void hexdump(void * data, uint8_t len) {
    uint8_t * bytes = (uint8_t *) data;
    const char * hex = "0123456789ABCDEF";

    for(uint8_t i = 0; i < len; i++) {
        usart_transmit(hex[(bytes[i] >> 4) & 0xF]);
        usart_transmit(hex[(bytes[i]) & 0xF]);
    }
}