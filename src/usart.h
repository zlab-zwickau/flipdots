
#ifndef __USART_H__
#define __USART_H__

#include <stdint.h>

void usart_init();
void usart_transmit(uint8_t data);
void print(const char* string);
void print_p(const char* string);
void print_int(int number, int base);
void print_bin(uint8_t number, uint8_t bits);
void println(char* string);
void hexdump(void * data, uint8_t len);

#endif